#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use rocket_contrib::json::Json;
use serde::Serialize;
use serde::Deserialize;
use std::sync::Mutex;
use std::collections::HashMap;
use rocket::{State};
use std::sync::atomic::{AtomicUsize, Ordering};
use rocket::http::Status;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct User {
    id: Option<usize>,
    name: String,
    email: String,
}

#[get("/<id>")]
fn get(id: usize, map: State<'_, Mutex<HashMap<usize, User>>>) -> Option<Json<User>> {
    // acquire lock in order to modify HashMap
    let hash_map = map.lock().unwrap();
    // retrieve value
    let user = hash_map.get(&id);
    // map into result
    user.map(|val| { Json(val.clone()) })
}

#[post("/", format = "json", data = "<input>")]
fn add(input: Json<User>, id_generator: State<AtomicUsize>, map: State<'_, Mutex<HashMap<usize, User>>>) -> Json<User> {
    // generate new id
    let val = id_generator.inner().fetch_add(1, Ordering::Relaxed);
    // get raw User out of input Json
    let mut user: User = input.into_inner();
    // store id within structure
    user.id = Some(val);
    // acquire lock in order to modify HashMap
    let mut hash_map = map.lock().unwrap();
    //insert value (note: clone as we need original one to be returned)
    hash_map.insert(val, user.clone());
    //return
    Json(user)
}

#[delete("/<id>")]
fn delete(id: usize, map: State<'_, Mutex<HashMap<usize, User>>>) -> Status {
    // acquire lock in order to modify HashMap
    let mut hashmap = map.lock().unwrap();
    // remove element, in case of success map it to Status::Ok, otherwise return default Status::NotFound
    hashmap.remove(&id).map_or(Status::NotFound, |_f| { Status::Ok })
}

fn main() {
    rocket::ignite()
        .manage(AtomicUsize::new(0))
        .manage(Mutex::new(HashMap::<usize, User>::new()))
        .mount("/", routes![get, add, delete])
        .launch();
}
